import copy
import sys

import pandas
from mainUI import *
from network import Network
from network import pandasModel

app = QApplication(sys.argv)

FormLog = QWidget()
uiLog = Ui_Dialog_start_Login()
uiLog.setupUi(FormLog)

FormReg = QWidget()
uiReg = Ui_Dialog_start_Reg()
uiReg.setupUi(FormReg)
# FormReg.show()

FormErr = QWidget()
uiErr = Ui_Dialog_show_error()
uiErr.setupUi(FormErr)

FormChoise = QWidget()
uiChoise = Ui_Dialog_Choise()
uiChoise.setupUi(FormChoise)

FormOrder = QWidget()
uiOrder = Ui_Dialog_Order()
uiOrder.setupUi(FormOrder)

N = Network
status = str(N.connect(N))
if status != '1':
    FormErr.show()

if status == '1':
    FormLog.show()


def timer_Err():
    uiErr.label_Err.setText("Reconnecting")
    status = str(N.reconnect(N))
    print(status)
    if status == '1':
        FormErr.hide()
        FormChoise.hide()
        FormOrder.hide()
        FormReg.hide()
        FormLog.show()


stErr = uiErr.pushButton_OK.clicked.connect(timer_Err)
print(status)

df = pandas.DataFrame()
dfs = df
dfch = df


def InitChoise():
    global status
    # try:
    status = N.send(N, 'c')
    print('Choice status c:', status)
    global df, dfs, dfch
    df = N.recv_Obj(N)
    dfch = dfs = df
    model = pandasModel(df)
    uiChoise.tableView.setModel(model)
    uiChoise.tableView.resizeColumnsToContents()
    uiChoise.tableView.resizeRowsToContents()

    uiChoise.comboBox_Arr.addItem('')
    uiChoise.comboBox_Dep.addItem('')

    df_uArr = df['Arr'].unique()
    df_uDep = df['Dep'].unique()

    uiChoise.comboBox_Arr.addItems(df_uArr)
    uiChoise.comboBox_Dep.addItems(df_uDep)

    # except Exception as e:
    #    print('Error:', e)


Tries = 0


def Login():
    global Tries
    global status
    global adminemode
    adminemode = False
    print(adminemode)
    try:
        status = N.send_R(N, 'l', status)
        if status == '-1':
            raise ValueError
        print('Choice status:', status)
        # status = N.send_R('l'.encode(),status).decode()
        if uiLog.lineEdit_user_id.text() == '':
            print('Please enter your login')
            FormErr.resize(200, 80)
            uiErr.label_Err.setText("Please enter your login")
            uiErr.pushButton_OK.hide()
            FormErr.show()
            return
        elif uiLog.lineEdit_password.text() == '':
            print('Please enter your password')
            FormErr.resize(200, 80)
            uiErr.label_Err.setText("Please enter your password")
            uiErr.pushButton_OK.hide()
            FormErr.show()
            return
        elif uiLog.lineEdit_user_id.text() == uiLog.lineEdit_password.text():
            FormErr.resize(300, 80)
            uiErr.label_Err.setText("Username and password shouldn`t be the same")
            uiErr.pushButton_OK.hide()
            FormErr.show()
            print('Username and password shouldn`t be the same')
            return
        else:
            status = N.send_R(N, uiLog.lineEdit_user_id.text(), status)
            if status == '-1':
                raise ConnectionError
            print('Receiving status: ', status)
            status = N.send_R(N, uiLog.lineEdit_password.text(), status)
            if status == '-1':
                raise ConnectionError
            print('Login status: ', status)
            log_st = status
            if log_st == '1':
                # Successful login
                pass
            elif log_st == 'adm':
                # Admin Mode
                pass
            elif log_st == '2':
                # Unsuccessful login
                if Tries >= 4:
                    uiErr.label_Err.setText('Too many failed attempts')
                    FormErr.resize(200, 80)
                    uiErr.pushButton_OK.hide()
                    FormErr.show()
                    FormLog.hide()
                uiLog.label_success.setText('Unsuccessful login')
                uiLog.label_success.show()
                Tries += 1
                status = str(N.reconnect(N))
            else:
                print('Connection error')
                FormErr.show()



    except Exception as e:
        print('Error:', e)
        FormErr.show()
        return
    print('Log_st - ', log_st)
    if log_st == 'adm' or log_st == '1':
        FormLog.hide()
        FormChoise.show()
        # Successful login
        InitChoise()


def InitRegWindow():
    FormReg.show()
    FormLog.hide()


def Reg():
    global status
    try:
        status = N.send_R(N, 'r', status)
        if status == '-1':
            raise ConnectionError
    except Exception as e:
        status = N.send_R(N, 'err', status)
        FormErr.resize(300, 80)
        uiErr.label_Err.setText('Error: ' + str(e))
        uiErr.pushButton_OK.hide()
        FormErr.show()
        print('Error: ' + e)
        return

    print('Choice status:', status)
    FName = uiReg.lineEdit_fname.text().capitalize().strip()
    SName = uiReg.lineEdit_sname.text().capitalize().strip()
    Log = uiReg.lineEdit_user_id.text().strip()
    print(FName, SName, Log, uiReg.lineEdit_password.text(), uiReg.lineEdit_Cpassword.text())
    if FName == '' or SName == '' or Log == '' or uiReg.lineEdit_password.text() == '' or uiReg.lineEdit_Cpassword.text() == '':
        print('Please enter all data')
        FormErr.resize(200, 80)
        uiErr.label_Err.setText("Please enter all data")
        uiErr.pushButton_OK.hide()
        FormErr.show()
        return
    elif uiReg.lineEdit_user_id.text().isdigit():
        FormErr.resize(260, 80)
        uiErr.label_Err.setText("Login shouldn`t be only digits")
        uiErr.pushButton_OK.hide()
        FormErr.show()
        return
    elif uiReg.lineEdit_password.text() == uiReg.lineEdit_user_id.text():
        print('Login and pass shouldn`t be the same')
        FormErr.resize(300, 80)
        uiErr.label_Err.setText("Login and pass shouldn`t be the same")
        uiErr.pushButton_OK.hide()
        FormErr.show()
        return
    elif uiReg.lineEdit_user_id.text() == uiLog.lineEdit_password.text():
        FormErr.resize(300, 80)
        uiErr.label_Err.setText("Username and password shouldn`t be the same")
        uiErr.pushButton_OK.hide()
        FormErr.show()
        print('Username and password shouldn`t be the same')
        return
    elif uiReg.lineEdit_password.text() != uiReg.lineEdit_Cpassword.text():
        FormErr.resize(360, 80)
        uiErr.label_Err.setText("Password and confirm don`t match.")
        uiErr.pushButton_OK.hide()
        FormErr.show()
        return
    elif not uiReg.lineEdit_fname.text().isalpha() or not uiReg.lineEdit_sname.text().isalpha():
        FormErr.resize(360, 80)
        uiErr.label_Err.setText("First and Last Name should be literals")
        uiErr.pushButton_OK.hide()
        FormErr.show()
        return
    else:

        try:
            status = N.send_R(N, FName, status)
            status = N.send_R(N, SName, status)
            status = N.send_R(N, Log, status)
            status = N.send_R(N, uiReg.lineEdit_password.text(), status)
            print(status)
        except Exception as e:
            FormErr.resize(360, 80)
            uiErr.label_Err.setText('Error: ' + e)
            uiErr.pushButton_OK.hide()
            FormErr.show()
            print('Error: ' + e)
            return
        if status == '1':
            FormLog.show()
            FormReg.hide()
            return
        elif status == '3':
            FormErr.resize(300, 80)
            uiErr.label_Err.setText('This username is already exist')
            uiErr.pushButton_OK.hide()
            FormErr.show()
        else:
            FormErr.resize(300, 80)
            uiErr.label_Err.setText('Connection Error')
            uiErr.pushButton_OK.hide()
            FormErr.show()
        uiReg.lineEdit_fname.setText('')
        uiReg.lineEdit_sname.setText('')
        uiReg.lineEdit_user_id.setText('')
        uiReg.lineEdit_password.setText('')
        uiReg.lineEdit_Cpassword.setText('')


#def Search():
#    global df
#    global dfs
#    global dfch
#    dfs = df
#    try:
#        if uiChoise.lineEdit_find.text() != '':
#            val = uiChoise.lineEdit_find.text()
#            dfs = df[(dfch.Name == val) | (dfch.Author_Name == val)]
#        else:
#            pass#dfs = df

#    except Exception as e:
#        print("Error: ", e)
#    model = pandasModel(dfs)
#    uiChoise.tableView.setModel(model)
#    uiChoise.tableView.resizeColumnsToContents()
#    uiChoise.tableView.resizeRowsToContents()


def ChangeTable():
    global df
    global dfs
    global dfch
    dfch = dfs
    try:
        if uiChoise.comboBox_Arr.currentText() != '':
            dfch = dfch.loc[df['Arr'] == uiChoise.comboBox_Arr.currentText()]

        if uiChoise.comboBox_Dep.currentText() != '':
            dfch = dfch.loc[df['Dep'] == uiChoise.comboBox_Dep.currentText()]




    except Exception as e:
        print("Error: ", e)
    model = pandasModel(dfch)
    uiChoise.tableView.setModel(model)
    uiChoise.tableView.resizeColumnsToContents()
    uiChoise.tableView.resizeRowsToContents()


index = ''

Flight_id=''
def toBook():
    global df
    global dfch
    global index
    global Flight_id
    index = uiChoise.tableView.selectionModel().currentIndex().row()
    print("Index - ", index)
    FormOrder.show()
    Flight_id = int(uiChoise.tableView.model().data(uiChoise.tableView.model().index(index,0)))
    print("Flight_id - ", Flight_id)
    Arr = df[df['Flight_id'] == Flight_id]['Arr'].iloc[0]
    print(df[df['Flight_id'] == Flight_id]['av_tickets'].iloc[0])
    Dep = df[df['Flight_id'] == Flight_id]['Dep'].iloc[0]
    Arr_d = df[df['Flight_id'] == Flight_id]['Arr_d'].iloc[0]
    Arr_t = df[df['Flight_id'] == Flight_id]['Arr_t'].iloc[0]
    Dep_d = df[df['Flight_id'] == Flight_id]['Dep_d'].iloc[0]
    Dep_t = df[df['Flight_id'] == Flight_id]['Dep_t'].iloc[0]
    Arr_time = "Час вильоту - " + str(Arr_d) + ' ' + str(Arr_t)
    Dep_time = "Час прибуття - " + str(Dep_d) + ' ' + str(Dep_t)
    Price = df[df['Flight_id'] == Flight_id]['Price'].iloc[0]
    print(Arr, '-', Dep, '\nArr -', Arr_time, '\nDep -', Dep_time, '\nPrice -', Price)
    # uiOrder.textBrowser_Details.setText(Name)
    # status = N.send(N,'a')

    # status = N.send_R(N,str(index), status)

    # descr = N.recv(N)

    strPrice = "\nВартість - " + str(Price) + "$"
    #uiOrder.label_Price.setText(strPrice)
    uiOrder.label_time_arr.setText(Arr_time)
    uiOrder.label_time_dep.setText(Dep_time)
    strDetails = 'Місце відправлення - ' + Arr + '\nМісце прибуття - ' + Dep + strPrice
    uiOrder.textBrowser_Details.setText(strDetails)


def Order():
    global index
    status = N.send(N, 'o')

    status = N.send_R(N, str(Flight_id), status)

    status = N.recv(N)
    if status == '1':
        uiErr.label_Err.setText('Success')
        uiErr.pushButton_OK.hide()
        FormErr.setWindowTitle("Info")
        FormErr.show()
    else:
        FormErr.show()


res = uiLog.pushButton_Reg.clicked.connect(InitRegWindow)
res = uiReg.pushButton_Reg.clicked.connect(Reg)
res = uiChoise.pushButton_Choise.clicked.connect(ChangeTable)
# res = uiChoise.pushButton_Search.clicked.connect(Search)
res = uiLog.pushButton_Login.clicked.connect(Login)
res = uiChoise.pushButton_toBook.clicked.connect(toBook)
res = uiOrder.pushButton_order.clicked.connect(Order)
print(res)

# uiChoise.comboBox_labg.activated[str].connect(onChanged)
# uiChoise.comboBox_vidav.activated[str].connect(onChanged)
# uiChoise.comboBox_year.activated[str].connect(onChanged)
# uiChoise.comboBox_Genre.activated[str].connect(onChanged)

# main()
if __name__ == '__main__':
    sys.exit(app.exec_())
