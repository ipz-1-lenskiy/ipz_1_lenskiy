import sqlite3 as sql
import pandas as pd


class DataBase(object):
    admin_st=''
    def start_conn(self):

        try:
            self.cnxn = sql.connect('DB.db', check_same_thread=False)
        except Exception as e:
            self.Error(self.e)
        cursor = self.cnxn.cursor()
        self.start_check(self)
    def start_check(self):
        try:
            self.cursor = self.cnxn.cursor()
        except Exception as e:
            self.Error(self,e)
        print(pd.read_sql("SELECT * FROM Login", self.cnxn))

    def SearchLog(self, login, password):
        print(login, password)
        try:df = pd.read_sql("SELECT L.login,P.password FROM Login as L "
                             "INNER JOIN Login_p as P on L.User_id = P.User_id ", self.cnxn)
        except Exception as e:
            self.Error(self,e)

        print(df)
        self.login = login
        reslog = False
        try:
            passcheck = df[df['login'] == login]['password'].iloc[0]
            if passcheck == password:
                reslog = True
                self.admin_st = int(df[df['login'] == login]['admin_st'].iloc[0])
                if self.admin_st == 1:
                    reslog = 'adm'
            print("Result: ", reslog,'\nPass: ', passcheck,'\nAdmin Mode:', self.admin_st)
            return reslog
        except:
            return reslog
    def ShowChoiseDef(self):
        try:
            self.df = pd.read_sql("SELECT * FROM FLIGHTS", self.cnxn)
            return self.df
        except Exception as e:
            self.Error(self, e)
            return -1

    def Order(self, index):
        try:
            login = self.login
            df = self.df
            print(df)
            av_tickets = int(df[df['Flight_id']==int(index)]['av_tickets'].iloc[0])
            print(av_tickets)
            self.cursor.execute(f"UPDATE Flights SET av_tickets={av_tickets-1} where Flight_id={(index)}")
            self.cnxn.commit()
            self.cursor.execute(f"INSERT INTO Orders (Login,Flight_id) values('{login}',{index})")
            self.cnxn.commit()
            return '1'
        except Exception as e:
            self.Error(self,e)
            return '-1'
    def Reg(self,Fname,Sname,Log,Pass):
        try:
            print(Fname,Sname,Log,Pass)
            df = pd.read_sql("SELECT login FROM Login", self.cnxn)
            print("df:")
            print(df)
            print("try:")
            try:
                print('Search res - ',df[df['login']==Log]['login'].iloc[0])
                print('3')
                return '3'
            except:
                print("except")
                self.cursor.execute(f"INSERT INTO Login(First_Name, Last_Name, login) values('{Fname}','{Sname}','{Log}')")
                print("ex1")
                self.cnxn.commit()
                print("commit1")
                self.cursor.execute(f"INSERT INTO Login_p(password) values('{Pass}')")
                print("ex2")
                self.cnxn.commit()
                print("1")
                return '1'
        except Exception as e:
            print("Exception as e -1")
            print(e)
            return '-1'
    def Error(self,e):
        print("Error in database: ", e)