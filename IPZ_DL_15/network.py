import socket
import pickle
from PySide2.QtCore import *



class Network:

    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server = "localhost"
    #server = "51.103.27.110"

    port = 9999
    addr = (server, port)

    def connect(self):
        try:
            self.client.connect(self.addr)
            self.client.send('Connect'.encode())
            return self.client.recv(2048).decode()
        except Exception as e:
            print("Error: ", e)
            return '-1'

    def send(self, data):
        try:
            self.client.send(data.encode())
            r = self.client.recv(2048).decode()
            if r == None:
                return -1
            return r
        except socket.error as e:
            print(e)
            return '-1'
    def send_R(self, data,status):
        if status == '1':
            try:
               self.client.send(data.encode())
               r=self.client.recv(2048).decode()
               if r == None:
                   return -1
               return (r)
            except socket.error as e:
                print(e)
                return '-1'
        else:print('Connection error')
    def recv(self):
        try:
            r=self.client.recv(2048).decode()
            if r == None:
                return -1
            return (r)
        except socket.error as e:
            print(e)
            return '-1'

    def recv_Obj(self):
        try:
            self.client.send('1'.encode())

            return pickle.loads(self.client.recv(24000))

            #data = self.client.recv(1024)
            #print(data)
            #pickled_data = pickle.loads(data)
            #print(pickled_data)
            #return pickled_data
        except socket.error as e:
            print(e)
            #return '-1'
    def reconnect(self):
        try:
            self.client.close()
            self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.client.connect(self.addr)
            self.client.send('Connect'.encode())
            return self.client.recv(2048).decode()
        except Exception as e:
            print("Error: ", e)
            return '-1'


class pandasModel(QAbstractTableModel):

    def __init__(self, data):
        QAbstractTableModel.__init__(self)
        self._data = data

    def rowCount(self, parent=None):
        return self._data.shape[0]

    def columnCount(self, parnet=None):
        return self._data.shape[1]

    def data(self, index, role=Qt.DisplayRole):
        if index.isValid():
            if role == Qt.DisplayRole:
                return str(self._data.iloc[index.row(), index.column()])
        return None

    def headerData(self, col, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self._data.columns[col]
        return None