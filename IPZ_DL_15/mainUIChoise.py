# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'mainUIChoise.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog_Choise(object):
    def setupUi(self, Dialog_Choise):
        if not Dialog_Choise.objectName():
            Dialog_Choise.setObjectName(u"Dialog_Choise")
        Dialog_Choise.setEnabled(True)
        Dialog_Choise.resize(620, 340)
        Dialog_Choise.setFocusPolicy(Qt.NoFocus)
        Dialog_Choise.setAutoFillBackground(False)
        Dialog_Choise.setWindowFlags(Qt.CustomizeWindowHint | Qt.WindowCloseButtonHint | Qt.MSWindowsFixedSizeDialogHint | Qt.WindowMinimizeButtonHint)


        self.line = QFrame(Dialog_Choise)
        self.line.setObjectName(u"line")
        self.line.setGeometry(QRect(-30, 320, 931, 20))
        self.line.setFrameShape(QFrame.HLine)
        self.line.setFrameShadow(QFrame.Sunken)
        self.label_success = QLabel(Dialog_Choise)
        self.label_success.setObjectName(u"label_success")
        self.label_success.setGeometry(QRect(200, 280, 47, 13))
        self.label_TimeR_St = QLabel(Dialog_Choise)
        self.label_TimeR_St.setObjectName(u"label_TimeR_St")
        self.label_TimeR_St.setGeometry(QRect(430, 50, 51, 31))
        self.label = QLabel(Dialog_Choise)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(10, 10, 121, 16))
        palette = QPalette()
        brush = QBrush(QColor(0, 85, 127, 255))
        brush.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.WindowText, brush)
        brush1 = QBrush(QColor(0, 0, 0, 255))
        brush1.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Inactive, QPalette.WindowText, brush1)
        brush2 = QBrush(QColor(120, 120, 120, 255))
        brush2.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Disabled, QPalette.WindowText, brush2)
        self.label.setPalette(palette)
        font = QFont()
        font.setFamily(u"Arial Black")
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setTextFormat(Qt.RichText)
        self.label.setScaledContents(False)
        self.tableView = QTableView(Dialog_Choise)
        self.tableView.setObjectName(u"tableView")
        self.tableView.setGeometry(QRect(10, 30, 471, 201))
        self.comboBox_Arr = QComboBox(Dialog_Choise)
        self.comboBox_Arr.setObjectName(u"comboBox_Arr")
        self.comboBox_Arr.setGeometry(QRect(10, 290, 111, 22))
        self.label_Arr= QLabel(Dialog_Choise)
        self.label_Arr.setObjectName(u"label_Arr")
        self.label_Arr.setGeometry(QRect(10, 260, 140, 20))
        self.comboBox_Dep = QComboBox(Dialog_Choise)
        self.comboBox_Dep.setObjectName(u"comboBox_Dep")
        self.comboBox_Dep.setGeometry(QRect(170, 290, 111, 22))
        self.label_Dep = QLabel(Dialog_Choise)
        self.label_Dep.setObjectName(u"label_Dep")
        self.label_Dep.setGeometry(QRect(170, 260, 120, 20))

        self.pushButton_Choise = QPushButton(Dialog_Choise)
        self.pushButton_Choise.setObjectName(u"pushButton_Choise")
        self.pushButton_Choise.setGeometry(QRect(300, 290, 71, 25))

        self.pushButton_toBook = QPushButton(Dialog_Choise)
        self.pushButton_toBook.setObjectName(u"pushButton_toBook")
        self.pushButton_toBook.setGeometry(QRect(490, 30, 110, 40))
        self.retranslateUi(Dialog_Choise)

        QMetaObject.connectSlotsByName(Dialog_Choise)
    # setupUi

    def retranslateUi(self, Dialog_Choise):
        Dialog_Choise.setWindowTitle(QCoreApplication.translate("Dialog_Choise", u"IPZ_Choice", None))
        self.label_success.setText("")
        self.label_TimeR_St.setText("")
        self.pushButton_toBook.setText("Забронювати")
        self.comboBox_Arr.show()
        self.comboBox_Dep.show()
        self.label.setText(QCoreApplication.translate("Dialog_Choise", u"IPZ_Airlines", None))
        self.label_Arr.setText(QCoreApplication.translate("Dialog_Choise", u"Відправлення", None))
        self.label_Dep.setText(QCoreApplication.translate("Dialog_Choise", u"Призначення", None))
        self.pushButton_Choise.setText(QCoreApplication.translate("Dialog_Choise", u"\u0412\u0438\u0431\u0440\u0430\u0442\u0438", None))
    # retranslateUi

